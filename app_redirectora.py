import socket
import random as r



if __name__ == "__main__":
    #Primero construir el tubo de conexion
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Af_INET es igual a ipv4 (adrees famili)

    #deja que use el puerto si no lo esta usando nadie
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    #socket escuha
    s.bind(('', 2346))# si dejamos el campo vacio se pueden conectar tanto de localhost comode ip

    #socket escucha#cuantas peticiones acepto
    s.listen(5)

    #aceptar peticiones
    while True:
        aleatorio = str(r.randint(11111, 99999))
        urls = [
            "https://www.google.com",
            "https://www.wikipedia.org",
            "https://www.github.com",
            "https://www.stackoverflow.com",
            "https://www.reddit.com",
            "https://www.amazon.com",
            "https://www.microsoft.com",
            "https://www.apple.com",
            "https://www.nytimes.com",
            "https://www.bbc.com",
            "https://www.cnn.com",
            "https://www.facebook.com",
            "https://www.twitter.com",
            "https://www.linkedin.com",
            "https://www.instagram.com",
            "https://www.tiktok.com",
            "https://www.netflix.com",
            "https://www.spotify.com",
            "https://www.medium.com",
            "https://www.python.org",
            "http://" + aleatorio
        ]
        random_url = r.choice(urls)

        response = (
            "HTTP/1.1 200 OK\r\n"
            "Content-Type: text/html\r\n\r\n"
            f"<html><body><h1>Redirecting to <a href='{random_url}'>Dame otra</a></h1></body></html>"
        )

        (rec_socket, address )= s.accept() #c es el tubo de comunicacion
        #leo un limite , hasta 2 KB
        received = rec_socket.recv(2048)
        print(received)

        #Contesto
        rec_socket.send(response.encode("utf-8"))
        rec_socket.close